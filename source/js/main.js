import '../css/style.scss'

import $ from 'jquery'
//-import 'slick-carousel'
import ViewPort from './modules/module.viewport'
//-import Tab from './modules/module.tab'
import './modules/module.form'
import './modules/module.helper'
import Popup from './modules/module.popup'
import objectFitImages from 'object-fit-images';
//import DrugNDrop from './modules/module.drugndrop'
import SimpleBar from 'simplebar';

// import 'lightgallery'
// import 'lg-video'

/*let drugndrop = new DrugNDrop({
	autoupload: true,
	url: 'http://test.maestros.ru/test.php',
	onFileAdded:(a, b)=>{
		//console.info(a, b)
	}
});*/

window.app = window.app || {};

$(()=>{

	objectFitImages('.img-fit');
	window.app.popup = new Popup({
		bodyClass: 'is-popup',
		autoCloseOtherPopups: false
	});


	let $b = $('body'), app = {}, breakpoint;


	$('.js-scrollbar').each((i, e)=>{
		new SimpleBar(e);
	});

	// $b.on('click', '[data-remove]', function (e) {
	// 	e.stopPropagation();
	// 	drugndrop.remove($(this).attr('data-remove'));
	// });

	new ViewPort({
		'0': ()=>{
			breakpoint = 'mobile';
		},
		'1200': ()=>{
			breakpoint = 'desktop';
		}
	});


	$b
		.on('click', '[data-scroll]', function(e){
			e.preventDefault();
			let offset = breakpoint === 'mobile' ? 140 : 20;
			$('html,body').animate({scrollTop: $($(this).attr('data-scroll')).offset().top - offset}, 500);
		})
	;



});

